#include <iostream>
#include <string>
#include <vector>
#include <assert.h>
#include <random>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::string;

class Trie {
public:
    explicit Trie();
    void AddString(string s, int pattern_number);
    vector<int> FindPattern(string& text, vector<int>& indent);
    ~Trie();
private:
    struct Node {
        vector<Node*> go;
        Node* parent;
        Node* suff_link;
        Node* up;
        char char_to_parent;
        bool is_leaf;
        vector<int> leaf_pattern_number;
        int depth;

        explicit Node()
        {
            go.assign(26, nullptr);
            parent = suff_link = up = nullptr;
            char_to_parent = -1;
            is_leaf = false;
        }
    };
    Node* root;
    int count_patterns;
    vector<int> patterns_sizes;
    void trie_clean(Node* v);

    Node* getSuffLink(Node* v);
    Node* getLink(Node* v, char c);
    Node* getUp(Node* v);
};

Trie::Trie()
{
    root = new Node();
    root->depth = 0;
    count_patterns = 0;
}

Trie::~Trie()
{
    trie_clean(root);
}

void Trie::trie_clean(Node* v)
{
    for(int i = 0; i < v->go.size(); ++i) {
        if(v->go[i] != nullptr) {
            if(v->go[i]->depth > v->depth) {
                trie_clean(v->go[i]);
            }
        }
    }
    delete v;
}

void Trie::AddString(string s, int pattern_number)
{
    Node* current = root;
    for(int i = 0; i < s.size(); ++i) {
        char c = s[i] - 'a';
        if(current->go[c] == nullptr) {
            current->go[c] = new Node();
            current->go[c]->parent = current;
            current->go[c]->char_to_parent = c;
            current->go[c]->depth = current->depth + 1;
        }
        current = current->go[c];
    }
    current->is_leaf = true;
    current->leaf_pattern_number.push_back(pattern_number);
    ++count_patterns;
    assert(patterns_sizes.size() == pattern_number);
    patterns_sizes.push_back(static_cast<int>(s.size()));
}

Trie::Node* Trie::getSuffLink(Node *v)
{
    if(v->suff_link == nullptr) {
        if(v == root || v->parent == root) {
            v->suff_link = root;
        }
        else {
            v->suff_link = getLink(getSuffLink(v->parent), v->char_to_parent);
        }
    }
    return v->suff_link;
}

Trie::Node* Trie::getLink(Node *v, char c)
{
    if(v->go[c] == nullptr) {
        if(v == root) {
            v->go[c] = root;
        }
        else {
            v->go[c] = getLink(getSuffLink(v), c);
        }
    }
    return v->go[c];
}

vector<int> Trie::FindPattern(string& text, vector<int>& indent)
{
    vector<int> result;
    Node* current = root;
    int l = 0;
    char t = text[l];
    int i;
    for(i = 0; t != '\0' && t != '\n' && l < text.size(); ++i) {
        t = text[l++];
        result.push_back(0);
        char c = t - 'a';
        current = getLink(current, c);
        Node* tmp = (current->is_leaf)? current : getUp(current);
        while(tmp != root) {
            assert(tmp->is_leaf);
            for(int j = 0; j < tmp->leaf_pattern_number.size(); ++j) {
                int in = i - patterns_sizes[tmp->leaf_pattern_number[j]] + 1;
                int pos = in  - indent[tmp->leaf_pattern_number[j]];
                if(pos >= 0) {
                    ++result[pos];
                }
            }
            tmp = getUp(tmp);
        }
    }
    return result;
}

Trie::Node* Trie::getUp(Node *v)
{
    if(v->up == nullptr) {
        if(getSuffLink(v)->is_leaf) {
            v->up = getSuffLink(v);
        }
        else if(getSuffLink(v) == root) {
            v->up = root;
        }
        else {
            v->up = getUp(getSuffLink(v));
        }
    }
    return v->up;
}


int main() {
    Trie trie;
    string p;
    cin >> p;
    string text;
    cin >> text;

    vector<string> patterns;
    string pattern = "";
    vector<int> indent;

    if (p[0] != '?') {
        pattern += p[0];
        indent.push_back(0);
    }
    for (int i = 1; i < p.size(); ++i) {
        if (p[i] == '?' && p[i - 1] != '?') {
            patterns.push_back(pattern);
            pattern = "";
        } else if (p[i] != '?') {
            if (p[i - 1] == '?') {
                indent.push_back(i);
            }
            pattern += p[i];
        }
    }
    if (pattern != "") {
        patterns.push_back(pattern);
    }
    vector<int> rslt;
    if (patterns.size() == 0) {
        string text;
        cin >> text;
        for (int i = 0; i < text.size() - p.size() + 1; ++i) {
            cout << i << " ";
        }
        cout << endl;
    } else {
        for (int i = 0; i < patterns.size(); ++i) {
            trie.AddString(patterns[i], i);
        }

        vector<int> res = trie.FindPattern(text, indent);

        for (int i = 0; i < res.size(); ++i) {
            if (res[i] == patterns.size() && i + p.size() <= res.size()) {
                cout << i << " ";
                rslt.push_back(i);
            }
        }
        cout << endl << endl;
    }

    return 0;
}