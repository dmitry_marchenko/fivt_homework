#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::endl;

void str_to_pi(string& str, vector<size_t>& pi) {
    pi.resize(str.size(), 0); //вектор значений префикс-функции
    size_t pref = 0; //здесь храним значение префикс-функции последнего обработанного элемента
    for(size_t i = 1; i < str.size(); ++i) {
        pref = pi[i - 1]; //начальная длина бордера, который может подойти
        while(pref != 0 && str[pref] != str[i]) {
            pref = pi[pref - 1]; //обновляем длину бордера на максимальную, которая может подойти
        }
        if (str[pref] == str[i]) {
            ++pref; //увеличиваем значение префикс-функции при совпадении
        }
        pi[i] = pref;
    }
}

string pi_to_str(vector<size_t> pi) {
    string result = "a"; //первой всегда будет буква 'a'
    vector<bool> is_visited(26, false);
    for(size_t i = 1; i < pi.size(); ++i) {
        if(pi[i] == 0) {
            size_t j = pi[i - 1]; //проверяем значение префикс-функции у предыдущего элемента
            if(j == 0) {
                //выбираем минимальный символ, который не увеличит значение префикс-функции
                result += 'b';
            } else {
                is_visited.assign(26, false);
                while(j > 0) {
                    //находим все символы, которые могут увеличить значение префикс-функции
                    is_visited[result[j] - 'a'] = true;
                    j = pi[j - 1];
                }
                size_t k;
                for(k = 1; k < is_visited.size() && is_visited[k]; ++k);
                result += 'a' + k; //и выбираем минимальный из оставшихся
            }
        }
        else {
            //выбираем единственный подходящий символ (следует из определения префикс-функции)
            result += result[pi[i] - 1];
        }
    }
    return result;
}

int main() {
    vector<size_t> pi;
    size_t val;

    while(cin >> val) {
        pi.push_back(val);
    }

    cout << pi_to_str(pi) << endl;

    return 0;
}