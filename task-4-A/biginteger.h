#include <iostream>
#include <vector>
#include <string>

class BigInteger;
const BigInteger operator -(const BigInteger &, const BigInteger &);
const BigInteger operator +(const BigInteger &, const BigInteger &);
const BigInteger operator *(const BigInteger &, const BigInteger &);
const BigInteger operator /(const BigInteger &, const BigInteger &);

class BigInteger {
    std::vector<int> number_;
    bool negative_;
    BigInteger(const std::vector<int> &a, const bool &y) : number_(a), negative_(y) {}

    void remove_negative_to_0() {
        if (toModString() == "0") {
            negative_ = 0;
        }
        return;
    }
    std::string toModString() const {
        std::string s = "";
        size_t n = number_.size();
        for (size_t i = 0; i < n; i++) {
            s += (number_[i] + '0');
        }
        return s;
    }
    std::string sign() const {
        return negative_ == 0 ? "" : "-";
    }
    BigInteger division_by_2(const BigInteger &b) {
        std::vector<int> a = b.number_;
        int carry = 0;
        for (size_t i = 0; i < a.size(); i++) {
            int b = a[i] % 2;
            a[i] = (a[i] + carry * 10)/2;
            carry = b;
        }
        BigInteger ans(a, 0);
        a = reverse_module(ans);
        while (a.size() > 1 && a.back() == 0)
            a.pop_back();
        ans.number_ = a;
        a = reverse_module(ans);
        ans.number_ = a;
        ans.remove_negative_to_0();
        return ans;
    }
    void modules_addition(const BigInteger &b) {
        std::vector<int> a1 = reverse_module(*this);
        std::vector<int> a2 = reverse_module(b);
        int carry = 0;
        for (size_t i = 0; i < a1.size() || i < a2.size() || carry; ++i) {
            if (i == a1.size()) {
                a1.push_back (0);
            }
            a1[i] += carry + (i < a2.size() ? a2[i] : 0);
            carry = a1[i] >= 10;
            if (carry) {
                a1[i] -= 10;
            }
        }
        number_ = a1;
        number_ = reverse_module(*this);
        remove_negative_to_0();
    }
    void modules_substraction(const BigInteger &b) {
        BigInteger a = *this;
        std::vector<int> a1 = reverse_module(a);
        std::vector<int> a2 = reverse_module(b);
        int sign1 = a.negative_;
        int sign2 = b.negative_;
        if ((a1.size() == a2.size() && a.toModString() < b.toModString()) || a1.size() < a2.size()) {
            std::vector<int> c = a1;
            a1 = a2;
            a2 = c;
            sign1 = sign2;
        }
        int carry = 0;
        for (size_t i = 0; i < a2.size() || carry; ++i) {
            a1[i] -= carry + (i < a2.size() ? a2[i] : 0);
            carry = a1[i] < 0;
            if (carry)  a1[i] += 10;
        }
        while (a1.size() > 1 && a1.back() == 0)
            a1.pop_back();
        number_ = a1;
        negative_ = sign1;
        number_ = reverse_module(*this);
        remove_negative_to_0();
    }
    bool prepfuncomp(const BigInteger &b, int x) const {
        int n1 = number_.size();
        int n2 = b.number_.size();
        if (x == -1)
            return n1 == n2 ? toModString() > b.toModString() : n1 > n2;
        return n1 == n2 ? toModString() < b.toModString() : n2 > n1;
    }
public:
    std::vector<int> reverse_module(const BigInteger &b) const {
        std::vector<int> ans = b.number_;
        int n = ans.size();
        for (int i = 0; i < n / 2; ++i) {
            int y = ans[i];
            ans[i] = ans[n - i - 1];
            ans[n - i - 1] = y;
        }
        return ans;
    }
    BigInteger() : negative_(0) {}
    BigInteger(const int &z) {
        negative_ = 0;
        int x = z;
        if (x < 0) {
            x = -1 * x;
            negative_ = 1;
        }
        if (x == 0) {
            number_.push_back (0);
        } else {
            while (x > 0) {
                number_.push_back (x % 10);
                x /= 10;
            }
            number_ = reverse_module(*this);
            remove_negative_to_0();
        }
    }
    BigInteger(const std::string &x,const std::string &sign = "") {
        int n = x.size();
        negative_ = 0;
        if (sign == "-") {
            negative_ = 1;
        }
        int i = 0;
        for (; i < n && x[i] == '0'; i++) {}
        if (i == n)
            number_.push_back (0);
        for (; i < n; i++) {

            number_.push_back (x[i] - '0');
        }
        remove_negative_to_0();
    }
    std::string toString() const {
        return sign() + toModString();
    }
    BigInteger & operator =(const BigInteger &b) {
        number_ = b.number_;
        negative_ = b.negative_;
        remove_negative_to_0();
        return *this;
    }
    bool operator <(const BigInteger &b) const {
        if (negative_ == 1 && b.negative_ == 1) {
            return prepfuncomp(b, -1);
        }
        if (negative_ == 0 && b.negative_ == 0) {
            return prepfuncomp(b, 1);
        }
        return (negative_ == 0 && b.negative_ == 1) ? 0 : 1;
    }
    bool operator >(const BigInteger &b) const {
        return b < *this;
    }
    bool operator ==(const BigInteger &b) const {
        bool ans = ((negative_ == b.negative_ && b.number_ == number_) || (b.toModString() == toModString() && toModString() == "0"));
        return ans;
    }
    bool operator <=(const BigInteger &b) const {
        return b == *this || b > *this;
    }
    bool operator >=(const BigInteger &b) const {
        return b == *this || b < *this;
    }
    bool operator !=(const BigInteger &b) const {
        return (*this == b) ? false : true;
    }
    explicit operator bool() const {
        return (*this == 0) ? false : true;
    }
    BigInteger operator -() const {
        BigInteger ans = *this;
        ans.negative_ = !negative_;
        ans.remove_negative_to_0();
        return ans;
    }
    BigInteger & operator +=(const BigInteger &b) {
        if (negative_ == b.negative_) {
            modules_addition(b);
        } else {
            modules_substraction(b);
        }
        remove_negative_to_0();
        return *this;
    }
    BigInteger & operator -=(const BigInteger &b) {
        *this += (-b);
        remove_negative_to_0();
        return *this;
    }
    BigInteger & operator ++() {
        remove_negative_to_0();
        return *this += 1;
    }
    BigInteger & operator --() {
        remove_negative_to_0();
        return *this -= 1;
    }
    const BigInteger operator ++(int) {
        BigInteger b = *this;
        ++*this;
        return b;
    }
    const BigInteger operator --(int) {
        BigInteger b = *this;
        --*this;
        return b;
    }
    BigInteger & operator *=(const BigInteger &b) {
        BigInteger c;
        c.negative_ = negative_^b.negative_;
        std::vector<int> a1 = reverse_module(*this);
        std::vector<int> a2 = reverse_module(b);
        int n1 = number_.size();
        c.number_.resize(number_.size() + b.number_.size());
        for (int i = 0; i < n1; ++i) {
            int carry=0;
            for (size_t j = 0; j < a2.size() || carry; ++j) {
                int cur = c.number_[i + j] + a1[i] * (j < a2.size() ? a2[j] : 0) + carry;
                c.number_[i+j] = cur % 10;
                carry = cur / 10;
            }
        }
        while (c.number_.size() > 1 && c.number_.back() == 0) {
            c.number_.pop_back();
        }
        c.number_ = reverse_module (c);
        *this = c;
        remove_negative_to_0();
        return *this;
    }
    BigInteger & operator /=(const BigInteger &b) {
        BigInteger c = b;
        c.negative_ = 0;
        int sign1 = negative_;
        int sign2 = b.negative_;
        negative_ = 0;
        int sign3 = sign1^sign2;
        BigInteger l = 0;
        BigInteger r = *this + 1;
        while (l + 1 < r) {
            BigInteger x = division_by_2(l + r);
            if (x * c <= *this) {
                l = x;
            } else {
                r = x;
            }
        }
        *this = l;
        negative_ = sign3;
        remove_negative_to_0();
        return *this;
    }
    BigInteger & operator %=(const BigInteger &b) {
        BigInteger c = *this - (*this / b) * b;
        *this = c;
        remove_negative_to_0();
        return *this;

    }
};
const BigInteger operator %(const BigInteger &b, const BigInteger &c) {
    BigInteger sum = b;
    return sum %= c;
}
const BigInteger operator /(const BigInteger &b, const BigInteger &c) {
    BigInteger sum = b;
    return sum /= c;
}
const BigInteger operator *(const BigInteger &b, const BigInteger &c) {
    BigInteger sum = b;
    return sum *= c;
}
const BigInteger operator +(const BigInteger &b, const BigInteger &c) {
    BigInteger sum = b;
    return sum += c;
}
const BigInteger operator -(const BigInteger &b, const BigInteger &c) {
    BigInteger sum = b;
    return sum -= c;
}
std::ostream & operator << (std::ostream &out, const BigInteger &b) {
    std::string ans = b.toString();
    return out << ans;
}
std::istream & operator >> (std::istream &in, BigInteger &b) {
    std::string s = "";
    in >> s;
    if (s[0] == '-') {
        s.erase(s.begin(), s.begin() + 1);
        b = BigInteger(s, "-");
    } else {
        b = BigInteger(s);
    }
    return in;
}