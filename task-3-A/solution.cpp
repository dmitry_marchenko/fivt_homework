#include <iostream>
#include <vector>
#include <functional>
#include <math.h>

using std::vector;
using std::cin;
using std::cout;
using std::endl;

#define ITER_CNT 54 // минимальная константа, проходящая все тесты :)

struct Point {
    double x;
    double y;
    double z;

    Point() : x(0), y(0), z(0) {}

    Point(double x_, double y_, double z_) : x(x_), y(y_), z(z_) {}

    Point operator+(Point other) {
        return Point(x + other.x, y + other.y, z + other.z);
    }

    Point operator-(Point& other) {
        return Point(x - other.x, y - other.y, z - other.z);
    }

    Point operator*(double n) {
        return Point(x * n, y * n, z * n);
    }
};

struct Segment {
    Point begin;
    Point end;

    Segment(Point point1, Point point2) : begin(point1), end(point2) {}
};

double sqr(double x) {
    return x * x;
}

std::pair<double, double> ternarySearchMin(std::function<double(Segment, Segment, double, double)> f,
                        double left, double right, Segment s1, Segment s2, double s) {
    for (int i = 0; i < ITER_CNT; ++i) {
        double a = (left * 2 + right) / 3;
        double b = (left + right * 2) / 3;
        double result_a = f(s1, s2, a, s);
        double result_b = f(s1, s2, b, s);
        if (result_a < result_b) {
            right = b;
        } else {
            left = a;
        }
    }
    return std::make_pair((left + right) / 2, f(s1, s2, (left + right) / 2, s));
}

/*
 * Точки отрезка АВ задаются уравнением E = tA+(1-t)B (0<=t<=1), A,B - вектора.
 * Соответственно F = sC + (1-s)D (0<=s<=1).
 * Пишем функцию расстояния(здесь - его квадрата) между точками, заданными этими уравнениями и
 * тернарным поиском находим её минимум.
 */

double dist(Segment segment1, Segment segment2, double t, double s) {
    Point point1 = segment1.begin * t + segment1.end * (1 - t);
    Point point2 = segment2.begin * s + segment2.end * (1 - s);

    return sqr(point1.x - point2.x) + sqr(point1.y - point2.y) + sqr(point1.z - point2.z);
}

double Distance(Segment segment1, Segment segment2) {
    double left = 0;
    double right = 1;
    double min_point1 = -1;
    for (int i = 0; i < ITER_CNT; ++i) {
        double a = (left * 2 + right) / 3;
        double b = (left + right * 2) / 3;
        std::pair<double, double> result_a = ternarySearchMin(dist, 0, 1, segment1, segment2, a);
        std::pair<double, double> result_b = ternarySearchMin(dist, 0, 1, segment1, segment2, b);
        if (result_a.second < result_b.second) {
            right = b;
            min_point1 = result_a.first;
        } else {
            left = a;
            min_point1 = result_b.first;
        }
    }
    double min_point2 = (left + right) / 2;
    return sqrt(dist(segment1, segment2, min_point1, min_point2));
}

int main() {
    double x, y, z;
    Point points[4];
    for (int i = 0; i < 4; ++i) {
        cin >> x >> y >> z;
        points[i] = Point(x, y, z);
    }

    Segment segment1(points[0], points[1]);
    Segment segment2(points[2], points[3]);

    cout << std::fixed;
    cout.precision(8);

    cout << Distance(segment1, segment2) << endl;

    return 0;
}