#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::endl;

void str_to_pi(const string& str, vector<int>& pi) { //из строки в префикс-функцию
    pi.resize(str.size(), 0); //вектор значений префикс-функции
    int pref = 0; //здесь храним значение префикс-функции последнего обработанного элемента
    for(int i = 1; i < str.size(); ++i) {
        pref = pi[i - 1]; //начальная длина бордера, который может подойти
        while(pref != 0 && str[pref] != str[i]) {
            pref = pi[pref - 1]; //обновляем длину бордера на максимальную, которая может подойти
        }
        if (str[pref] == str[i]) {
            ++pref; //увеличиваем значение префикс-функции при совпадении
        }
        pi[i] = pref;
    }
}

string pi_to_str(vector<int>& pi) { //из префикс-функии в минимальную строку
    string result = "a"; //первой всегда будет буква 'a'
    vector<bool> is_visited(26, false);
    for(int i = 1; i < pi.size(); ++i) {
        if(pi[i] == 0) {
            int j = pi[i - 1]; //проверяем значение префикс-функции у предыдущего элемента
            if(j == 0) {
                result += 'b'; //выбираем минимальный символ, который не увеличит значение
            } else {           //префикс-функции
                is_visited.assign(26, false);
                while(j > 0) {
                    is_visited[result[j] - 'a'] = true; //находим все символы, которые могут
                    j = pi[j - 1];                      //увеличить значение префикс-функции
                }
                int k;
                for(k = 1; k < is_visited.size() && is_visited[k]; ++k);
                result += 'a' + k; //и выбираем минимальный из оставшихся
            }
        }
        else {
            result += result[pi[i] - 1]; //выбираем единственный подходящий символ
        }                                //(следует из определения префикс-функции)
    }
    return result;
}


void z_to_pi(vector<int>& z, vector<int>& pi) { //из z-функции в префикс функцию
    pi.assign(z.size(), 0);
    for(int i = 1; i < z.size(); ++i) {
        for(int j = z[i] - 1; j >= 0; --j) {
            if(pi[i + j] > 0) {
                break;
            } else {
                pi[i + j] = j + 1;
            }
        }
    }
}

void str_to_z(string& str, vector<int>& z) { //из строки в z-функцию
    z.assign(str.size(), 0);
    z[0] = static_cast<int>(str.size());//иницилизируем начальные значения
    int left = 0;                       //границы самого правого найденного
    int right = 0;                      //блока
    for(int i = 1; i < z.size(); ++i) {
        if(i > right) {
            int j = 0;
            while(i + j < z.size() && str[j] == str[i + j]) { //наивно ищем z[i]
                ++j;                                          //
            }
            left = i;           //обновляем границы
            right = i + j - 1;  //
            z[i] = j;           //фиксируем результат
        } else {
            if(right < z[i - left] + i) {
                int j = right - i;
                while(i + j < z.size() && str[j] == str[i + j]) { //наивно досчитываем z[i]
                    ++j;                                          //
                }
                left = i;
                right = i + j - 1;
                z[i] = j;
            } else {
                z[i] = z[i - left]; //в данном случае значение z[i] уже известно
            }
        }
    }
}

void pi_to_z(vector<int>& pi, vector<int>& z) { //из префикс-функции в z-функцию
    string str = pi_to_str(pi);
    str_to_z(str, z);
}

string z_to_str(vector<int>& z) { //из z-функции в строку
    vector<int> pi;
    z_to_pi(z, pi);
    return pi_to_str(pi);
}

int main() {
    vector<int> z;
    int val;

    while(cin >> val) {
        z.push_back(val);
    }

    cout << z_to_str(z) << endl;

    return 0;
}