#include <iostream>
#include <vector>
#include <stack>
#include <math.h>
#include <map>
#include <algorithm>

using std::vector;
using std::stack;
using std::cin;
using std::cout;
using std::endl;
using std::sort;
using std::map;

struct Point {
    double x;
    double y;
    double z;

    Point() : x(0), y(0), z(0) {}

    Point(double x_, double y_, double z_) : x(x_), y(y_), z(z_) {}

    Point operator+(const Point& other) const{
        return Point(x + other.x, y + other.y, z + other.z);
    }

    Point operator-(const Point& other) const {
        return Point(x - other.x, y - other.y, z - other.z);
    }

    bool operator==(const Point& other) const {
        return x == other.x && y == other.y && z == other.z;
    }

    bool operator!=(const Point& other) const {
        return !operator==(other);
    }

    Point operator*(double n) const {
        return Point(x * n, y * n, z * n);
    }

    bool operator<(const Point& other) const {
        return x < other.x || (x == other.x && y < other.y) || (x == other.x && y == other.y && z < other.z);
    }
};

struct Face {
    vector<Point> vertices;

    Face(Point p1, Point p2, Point p3) {
        vertices.push_back(p1);
        vertices.push_back(p2);
        vertices.push_back(p3);
    }
    Face() {
        vertices.push_back(Point());
        vertices.push_back(Point());
        vertices.push_back(Point());
    }
    Point find_normal() const {
        Point a = vertices[0];
        Point b = vertices[1];
        Point c = vertices[2];
        Point v1 = b - a;
        Point v2 = c - a;

        double A = v1.y * v2.z - v2.y * v1.z;
        double B = v2.x * v1.z - v1.x * v2.z;
        double C = v1.x * v2.y - v2.x * v1.y;

        return Point(A, B, C);
    }

    bool operator==(const Face& other) const {
        Point normal = find_normal();

        double A = normal.x;
        double B = normal.y;
        double C = normal.z;
        double D = (-1) * (A * vertices[0].x + B * vertices[0].y + C * vertices[0].z);

        return A * other.vertices[0].x + B * other.vertices[0].y + C * other.vertices[0].z + D == 0 &&
               A * other.vertices[1].x + B * other.vertices[1].y + C * other.vertices[1].z + D == 0 &&
               A * other.vertices[2].x + B * other.vertices[2].y + C * other.vertices[2].z + D == 0;
    }

    bool operator!=(const Face& other) const {
        return !operator==(other);
    }
};


double sqr(double x) {
    return x * x;
}

/*
 * Возвращает квадрат угла p1-p2-p3.
 */

double sqr_sin(const Point& p1, const Point& p2, const Point& p3) {
    Point v1 = p1 - p2;
    Point v2 = p3 - p2;

    if (v1 == Point(0, 0, 0) || v2 == Point(0, 0, 0)) {
        return 0;
    }

    double result = (sqr(v1.y * v2.z - v2.y * v1.z) + sqr(v2.x * v1.z - v1.x * v2.z) +
                                                                    sqr(v1.x * v2.y - v2.x * v1.y)) /
            ((sqr(v1.x) + sqr(v1.y) + sqr(v1.z)) * (sqr(v2.x) + sqr(v2.y) + sqr(v2.z)));

    return result;
}

/*
 * Проверяет, находятся ни в одном полупространстве точки p1 и p2 относительно проскости,
 * заданной точками a, b, c.
 */

bool in_one_space(const Point& p1, const Point& p2, const Point& a, const Point& b, const Point& c) {
    Face f(a, b, c);
    Point normal = f.find_normal();

    double A = normal.x;
    double B = normal.y;
    double C = normal.z;
    double D = (-1) * (A * a.x + B * a.y + C * a.z);

    double res1 = A * p1.x + B * p1.y + C * p1.z + D;
    double res2 = A * p2.x + B * p2.y + C * p2.z + D;

    return (res1 > 0 && res2 > 0) || (res1 < 0 && res2 < 0);
}

/*
 * Возвращает начальную для построения грань выпуклой оболочки.
 */

Face find_first_face(const vector<Point>& points) {
    Face result;
    if (points.size() < result.vertices.size()) {
        result.vertices = points;
        return result;
    }
    result.vertices[0] = points[0];

    for (int i = 1; i < points.size(); ++i) {
        if (points[i].z < result.vertices[0].z ||
                (points[i].z == result.vertices[0].z && points[i].y < result.vertices[0].y) ||
                (points[i].z == result.vertices[0].z && points[i].y == result.vertices[0].y &&
                        points[i].x < result.vertices[0].x)) {
            result.vertices[0] = points[i];
        }
    }

    Point p = result.vertices[0] + Point(0, 0, 1);
    result.vertices[1] = points[0];

    for (int i = 1; i < points.size(); ++i) {
        if (points[i] != result.vertices[0] &&
                sqr_sin(p, result.vertices[0], points[i]) >
                        sqr_sin(p, result.vertices[0], result.vertices[1])) {
            result.vertices[1] = points[i];
        }
    }

    for (int i = 0; i < points.size(); ++i) {
        bool flag = true;
        if (points[i] != result.vertices[0] && points[i] != result.vertices[1]) {
            int k = 0;
            while (k == i || points[k] == result.vertices[0] || points[k] == result.vertices[1]) {
                ++k;
            }
            for (int j = 0; j < points.size(); ++j) {
                if (points[j] != result.vertices[0] && points[j] != result.vertices[1] && i != j &&
                    !in_one_space(points[k], points[j], result.vertices[0], result.vertices[1], points[i])) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                result.vertices[2] = points[i];
                break;
            }
        }
    }

    return result;
}

/*
 * Возвращает косинус угла между векторами v1 и v2.
 */

double cos_of_angle(const Point& v1, const Point& v2) {
    double scalar = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    double len1 = std::sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
    double len2 = std::sqrt(v2.x * v2.x + v2.y * v2.y + v2.z * v2.z);
    return scalar / (len1 * len2);
}

/*
 * Ищет смежную с face грань, соприкасающуюся по ребру [edge.first; edge.second].
 * Если такого ребра ещё нет среди ответа, то возвращает true и записывает результат в next.
 * Иначе возвращает false.
 */

bool find_incident_face(const Face& face, std::pair<int, int> edge, const vector<Point>& points, Face& next,
                        const vector<Face>& result) {
    Point normal = face.find_normal();
    int i = 0;
    while (points[i] == face.vertices[0] || points[i] == face.vertices[1] || points[i] == face.vertices[2]) {
        ++i;
    }

    if (i >= points.size()) {
        return false;
    }

    if (in_one_space(points[i], normal + face.vertices[0],
                     face.vertices[0], face.vertices[1], face.vertices[2])) {
        normal = normal * (-1);
    }

    next = Face(face.vertices[edge.first], face.vertices[edge.second], points[i]);
    Point next_normal = next.find_normal();
    if (in_one_space(face.vertices[(edge.second + 1) % 3], points[i] + next_normal,
                     next.vertices[0], next.vertices[1], next.vertices[2])) {
        next_normal = next_normal * (-1);
    }

    double current_cos = cos_of_angle(normal, next_normal);

    for (int j = i + 1; j < points.size(); ++j) {
        if (points[j] != face.vertices[0] && points[j] != face.vertices[1] && points[j] != face.vertices[2]) {
            Face check(face.vertices[edge.first], face.vertices[edge.second], points[j]);
            Point check_normal = check.find_normal();
            if (in_one_space(face.vertices[(edge.second + 1) % 3], points[j] + check_normal,
                             check.vertices[0], check.vertices[1], check.vertices[2])) {
                check_normal = check_normal * (-1);
            }
            double check_cos = cos_of_angle(normal, check_normal);
            if (check_cos > current_cos) {
                current_cos = check_cos;
                next = check;
            }
        }
    }

    for (int j = 0; j < result.size(); ++j) {
        if (next == result[j]) {
            return false;
        }
    }

    return true;
}

vector<Face> BuildConvexHull3D(const vector<Point>& points) {
    stack<Face> not_completed;
    vector<Face> result;
    Face first = find_first_face(points);
    not_completed.push(first);
    result.push_back(first);

    while (!not_completed.empty()) {
        Face current = not_completed.top();
        not_completed.pop();
        Face next;
        for (int i = 0; i < 3; ++i) {
            if(find_incident_face(current, std::make_pair(i, (i + 1) % 3), points, next, result)) {
                not_completed.push(next);
                result.push_back(next);
            }
        }
    }

    return result;
}

bool a_less_b(const int a, const int b, const Point& normal, const Face& f) {
    Point v1 = f.vertices[a] - f.vertices[(b + 1) % 3];
    Point v2 = f.vertices[b] - f.vertices[(b + 1) % 3];

    Point cross_product(v1.y * v2.z - v2.y * v1.z, v2.x * v1.z - v1.x * v2.z, v1.x * v2.y - v2.x * v1.y);

    return in_one_space(f.vertices[0] + normal, f.vertices[0] + cross_product,
                        f.vertices[0], f.vertices[1], f.vertices[2]);
}

vector<vector<int>> SortForTests(vector<Face>& faces, map<Point, int>& my_map,
                                 vector<Point> points) {
    vector<vector<int>> result;
    for (int i = 0; i < faces.size(); ++i) {

        Point normal = faces[i].find_normal();
        int j = 0;
        while (points[j] == faces[i].vertices[0] || points[j] == faces[i].vertices[1] ||
                points[j] == faces[i].vertices[2]) {
            ++j;
        }

        if (in_one_space(points[j], normal + faces[i].vertices[0], faces[i].vertices[0],
                         faces[i].vertices[1], faces[i].vertices[2])) {
            normal = normal * (-1);
        }

        for (j = 0; j < 3; ++j) {
            if (!a_less_b(j, (j + 1) % 3, normal, faces[i])) {
                std::swap(faces[i].vertices[j], faces[i].vertices[(j + 1) % 3]);
            }
        }

        int start = 0;
        for (j = 1; j < faces[i].vertices.size(); ++j) {
            if (my_map[faces[i].vertices[j]] < my_map[faces[i].vertices[start]]) {
                start = j;
            }
        }

        vector<int> res_of_face;

        for (j = 0; j < faces[i].vertices.size(); ++j) {
            res_of_face.push_back(my_map[faces[i].vertices[(start + j) % faces[i].vertices.size()]]);
        }

        result.push_back(res_of_face);
    }

    sort(result.begin(), result.end());

    return result;
}

int main() {
    int m;
    cin >> m;
    for (int k = 0; k < m; ++k) {
        int n;
        cin >> n;
        int x, y, z;
        vector<Point> points;
        map<Point, int> my_map;
        for (int i = 0; i < n; ++i) {
            cin >> x >> y >> z;
            points.push_back(Point(x, y, z));
            my_map[Point(x, y, z)] = i;
        }

        vector<Face> convex_hull = BuildConvexHull3D(points);

        cout << convex_hull.size() << endl;
        vector<vector<int>> to_output = SortForTests(convex_hull, my_map, points);

        for (int i = 0; i < to_output.size(); ++i) {
            cout << to_output[i].size() << " ";
            for (int j = 0; j < to_output[i].size(); ++j) {
                cout << to_output[i][j] << " ";
            }
            cout << endl;
        }
    }

    return 0;
}