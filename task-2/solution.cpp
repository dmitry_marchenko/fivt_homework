#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::endl;


#define ALF_SIZE 26ull //размер алфавита

void calc_groups(vector<size_t>& count) {  //подсчет концов групп в карманной сортировке
    for (size_t i = 1; i < count.size(); ++i) {
        count[i] += count[i - 1];
    }
}

void build_suff_array(string& s, vector<size_t>& suffArray) {
    s += '$';

    //карманная сортировка по первым символам
    vector<size_t> count(static_cast<size_t>(ALF_SIZE + 1), 0);
    for (size_t i = 0; i < s.size(); ++i) {
        ++count[(s[i] == '$')? 0 : s[i] - 'a' + 1];
    }

    calc_groups(count);
    vector<size_t> suffs(s.size());
    for (long long i = s.size() - 1; i >= 0; --i) {
        size_t ch = (s[i] == '$')? 0 : s[i] - 'a' + 1;
        suffs[--count[ch]] = i;
    }

    //подсчет классов эквивалентности по первому символу
    //в classes[i] лежит класс i-го суффикса
    vector<size_t> classes(s.size());
    size_t classesNumber = 0;
    char lastChar = '$';
    for (size_t i = 0; i < suffs.size(); ++i) {
        if (s[suffs[i]] != lastChar) {
            lastChar = s[suffs[i]];
            ++classesNumber;
        }
        classes[suffs[i]] = classesNumber;
    }

    //сортировка строк длины 2 * currentLen
    size_t currentLen = 1;
    while (currentLen <= s.size()) {
        //сортировка по второй половине
        vector<size_t> sortedBy2(s.size());
        for (size_t i = 0; i < sortedBy2.size(); ++i) {
            sortedBy2[i] = (suffs[i] - currentLen + s.size()) % s.size();
        }

        //сортировка по первой половине
        count.assign(s.size(), 0);
        for (size_t i = 0; i < sortedBy2.size(); ++i) {
            ++count[classes[sortedBy2[i]]];
        }
        calc_groups(count);
        for (long long i = sortedBy2.size() - 1; i >= 0; --i) {
            suffs[--count[classes[sortedBy2[i]]]] = sortedBy2[i];
        }

        //подсчет классов эквивалентности
        vector<size_t> newClasses(s.size());
        classesNumber = 0;
        for (size_t i = 1; i < suffs.size(); ++i) {
            size_t mid1 = (suffs[i] + currentLen) % s.size();
            size_t mid2 = (suffs[i - 1] + currentLen) % s.size();
            if (classes[suffs[i - 1]] != classes[suffs[i]] || classes[mid1] != classes[mid2]) {
                ++classesNumber;
            }
            newClasses[suffs[i]] = classesNumber;
        }
        classes.swap(newClasses);
        currentLen *= 2;
    }

    suffArray.swap(suffs);
}

void build_lcp(string& s, vector<size_t>& suffArray, vector<size_t>& lcp) {
    lcp.resize(s.size());
    vector<size_t> pos(s.size()); //массив, обратный суффиксному
    for (size_t i = 0; i < pos.size(); ++i) {
        pos[suffArray[i]] = i;
    }

    size_t k = 0;
    for (size_t i = 0; i < pos.size(); ++i) {
        if (pos[i] == pos.size() - 1) {
            lcp[pos.size() - 1] = 0;
            k = 0;
        }
        else {
            size_t j = suffArray[pos[i] + 1];
            while (std::max(i + k, j + k) < pos.size() && s[i + k] == s[j + k]) {
                ++k;
            }
            lcp[pos[i]] = k;
        }
        if (k > 0) {
            --k;
        }
    }
}

size_t count_diff_substr(string& s) {
    vector<size_t> suffArray;
    build_suff_array(s, suffArray);

    vector<size_t> lcp;
    build_lcp(s, suffArray, lcp);

    //вычисление результата
    size_t result = s.size() - 1 - suffArray[0];

    for (size_t i = 1; i < suffArray.size(); ++i) {
        result += s.size() - 1 - suffArray[i] - lcp[i - 1];
    }

    return result;
}

int main() {

    string s;
    cin >> s;

    cout << count_diff_substr(s) << endl;

    return 0;
}