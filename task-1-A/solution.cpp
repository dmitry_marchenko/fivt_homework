#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::endl;

void find_pattern(const string& text, const string& pat) {
    string pattern = pat;
    pattern += '$';
    vector<size_t> pi(pattern.size(), 0); //вектор значений префикс-функции для образца
    size_t pref = 0; //здесь храним значение префикс-функции последнего обработанного элемента
    for(size_t i = 1; i < pattern.size(); ++i) {
        pref = pi[i - 1]; //начальная длина бордера, который может подойти
        while(pref != 0 && pattern[pref] != pattern[i]) {
            pref = pi[pref - 1]; //обновляем длину бордера на максимальную, которая может подойти
        }
        if (pattern[pref] == pattern[i]) {
            ++pref; //увеличиваем значение префикс-функции при совпадении
        }
        pi[i] = pref;
    }

    for(size_t i = 0; i < text.size(); ++i) {
        while(pref != 0 && pattern[pref] != text[i]) {
            pref = pi[pref - 1];
        }
        if(pattern[pref] == text[i]) {
            ++pref;
        }
        //если значение префикс-функции равно длине
        //образца, то нашли вхождение и выводим ответ
        if(pref == pattern.size() - 1) {
            cout << i - pattern.size() + 2 << ' ';
        }
    }
}

int main() {
    string pattern, text;
    cin >> pattern >> text;

    find_pattern(text, pattern);

    return 0;
}