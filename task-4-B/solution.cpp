#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <queue>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::queue;

class Game {
private:

    enum Result {
        Win, Lose, Undefined
    };

    struct point {
        char x;
        char y;

        point(char x_, char y_) : x(x_), y(y_) {}
        point() : x(0), y(0) {}
    };

    struct vertex {
        point qw;
        point kb;
        bool black_move;

        Result res;
        int depth;
        int distance;
        vector<int> parents;
        vector<int> children;

        vertex(char qw_x, char qw_y, char kb_x, char kb_y, bool black_move_) {
            qw = point(qw_x, qw_y);
            kb = point(kb_x, kb_y);
            black_move = black_move_;
            res = Undefined;
            depth = 0;
        }
    };

    const point kw = point(2, 2);

    vector<vertex> positions;
    map<int, int> pos_num;
    queue<int> q;

    bool is_check(vertex& pos);
    bool is_checkmate(vertex& pos);
    bool is_valid(vertex& pos);
    bool check_bounds(point& piece);
    bool is_stalemate(vertex& pos);
    bool qw_lose(vertex& pos);
    int num_of_pos(vertex& pos);

public:
    Game();
    int DistanceToCheckmate(char qw_x, char qw_y, char kb_x, char kb_y, bool black_move);
};

bool Game::is_check(Game::vertex &pos) {
    bool check_on_x = pos.qw.x == pos.kb.x &&
            (pos.qw.x != kw.x || (kw.y > pos.qw.y && kw.y > pos.kb.y) ||
                                 (kw.y < pos.qw.y && kw.y < pos.kb.y));
    bool check_on_y = pos.qw.y == pos.kb.y &&
            (pos.qw.y != kw.y || (kw.x > pos.qw.x && kw.x > pos.kb.x) ||
                                 (kw.x < pos.qw.x && kw.x < pos.kb.x));
    bool check_on_diag1 = pos.qw.x - pos.qw.y == pos.kb.x - pos.kb.y &&
            (pos.qw.x - pos.qw.y != kw.x - kw.y ||
                    (kw.x < pos.qw.x && kw.y < pos.qw.y && kw.x < pos.kb.x && kw.y < pos.kb.y) ||
                    (kw.x > pos.qw.x && kw.y > pos.qw.y && kw.x > pos.kb.x && kw.y > pos.kb.y));
    bool check_on_diag2 = pos.qw.x + pos.qw.y == pos.kb.x + pos.kb.y &&
            (pos.qw.x + pos.qw.y != kw.x + kw.y ||
                    (kw.x < pos.qw.x && kw.y > pos.qw.y && kw.x < pos.kb.x && kw.y > pos.kb.y) ||
                    (kw.x > pos.qw.x && kw.y < pos.qw.y && kw.x > pos.kb.x && kw.y < pos.kb.y));
    return (check_on_x || check_on_y || check_on_diag1 ||check_on_diag2) /*&& pos.black_move*/;
}

bool Game::check_bounds(point &piece) {
    return piece.x >= 0 && piece.x <= 7 && piece.y >= 0 && piece.y <= 7;
}

bool Game::is_valid(vertex &pos) {
    bool qw_kw_collision = pos.qw.x == kw.x && pos.qw.y == kw.y;
    bool qw_kb_collision = pos.qw.x == pos.kb.x && pos.qw.y == pos.kb.y;
    bool kb_kw_collision = pos.kb.x - kw.x <= 1 && pos.kb.x - kw.x >= -1 &&
                           pos.kb.y - kw.y <= 1 && pos.kb.y - kw.y >= -1;
    bool pieces_at_board = check_bounds(pos.qw) && check_bounds(pos.kb);
    bool valid_move = !(is_check(pos) && !pos.black_move);

    return !qw_kw_collision && !qw_kb_collision && !kb_kw_collision && pieces_at_board && valid_move;
}

bool Game::is_checkmate(Game::vertex &pos) {
    if (!pos.black_move || !is_valid(pos) || qw_lose(pos)) {
        return false;
    }
    for (char x = -1; x <= 1; ++x) {
        for (char y = -1; y <= 1; ++y) {
            vertex new_pos = vertex(pos.qw.x, pos.qw.y, pos.kb.x + x, pos.kb.y + y, pos.black_move);
            if (!is_check(new_pos) && is_valid(new_pos)) {
                return false;
            }
        }
    }
    return true;
}

bool Game::is_stalemate(vertex &pos) {
    if (!pos.black_move || is_check(pos)) {
        return false;
    }
    for (char x = -1; x <= 1; ++x) {
        for (char y = -1; y <= 1; ++y) {
            if (x != 0 || y != 0) {
                vertex new_pos = vertex(pos.qw.x, pos.qw.y, pos.kb.x + x, pos.kb.y + y, pos.black_move);
                if (!is_check(new_pos) && is_valid(new_pos)) {
                    return false;
                }
            }
        }
    }
    return true;
}

bool Game::qw_lose(vertex &pos) {
    bool qw_under_attack = pos.kb.x - pos.qw.x <= 1 && pos.kb.x - pos.qw.x >= -1 &&
                           pos.kb.y - pos.qw.y <= 1 && pos.kb.y - pos.qw.y >= -1;
    bool kw_is_far = pos.qw.x - kw.x > 1 || pos.qw.x - kw.x < -1 ||
                     pos.qw.y - kw.y > 1 || pos.qw.y - kw.y < -1;
    return qw_under_attack && kw_is_far && pos.black_move;
}

int Game::num_of_pos(vertex &pos) {
    return 2 * (64 * (8 * pos.qw.x + pos.qw.y) + (8 * pos.kb.x + pos.kb.y)) + ((pos.black_move)? 1 : 0);
}

Game::Game() {
    for (char qw_x = 0; qw_x < 8; ++qw_x) {
        for (char qw_y = 0; qw_y < 8; ++qw_y) {
            for (char kb_x = 0; kb_x < 8; ++kb_x) {
                for (char kb_y = 0; kb_y < 8; ++kb_y) {
                    for (int black_move = 0; black_move < 2; ++black_move) {
                        vertex new_pos(qw_x, qw_y, kb_x, kb_y, static_cast<bool>(black_move));
                        if (is_valid(new_pos)) {
                            int num = num_of_pos(new_pos);
                            pos_num[num] = static_cast<int>(positions.size());
                            positions.push_back(new_pos);
                            if (is_checkmate(new_pos)) {
                                q.push(static_cast<int>(positions.size() - 1));
                                positions.back().res = Lose;
                                positions.back().distance = 0;
                            } else if (is_stalemate(new_pos) || qw_lose(new_pos)) {
                                positions.back().res = Win;
                            }
                        }
                    }
                }
            }
        }
    }

    for (int i = 0; i < positions.size(); ++i) {
        if (!positions[i].black_move) {
            for (char x = -1; x <= 1; ++x) {
                for (char y = -1; y <= 1; ++y) {
                    if (x != 0 || y != 0) {
                        vertex prev_pos = vertex(positions[i].qw.x, positions[i].qw.y,
                                         positions[i].kb.x + x, positions[i].kb.y + y,
                                         !positions[i].black_move);
                        if (is_valid(prev_pos) && !is_checkmate(prev_pos) && !is_stalemate(prev_pos) &&
                                                                             !qw_lose(prev_pos)) {
                            int prev_num = pos_num[num_of_pos(prev_pos)];
                            positions[i].parents.push_back(prev_num);
                            ++positions[prev_num].depth;
                            positions[prev_num].children.push_back(i);
                        }
                    }
                }
            }
        } else {
            if (is_stalemate(positions[i]) || qw_lose(positions[i])) {
                positions[i].parents.push_back(i);
                positions[i].children.push_back(i);
            }
            vector<std::pair<char, char>> delta = {{1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1},
                                                   {-1, 0}, {-1, 1}, {0, 1}};
            for (char j = 0; j < delta.size(); ++j) {
                for (char d = 1; d < 8; ++d) {
                    vertex prev_pos = vertex(positions[i].qw.x + d * delta[j].first,
                                             positions[i].qw.y + d * delta[j].second,
                                             positions[i].kb.x, positions[i].kb.y,
                                             !positions[i].black_move);
                    if ((prev_pos.qw.x == kw.x && prev_pos.qw.y == kw.y) ||
                        (prev_pos.qw.x == prev_pos.kb.x && prev_pos.qw.y == prev_pos.kb.y) ||
                        !check_bounds(prev_pos.qw)) {
                        break;
                    }
                    if (is_valid(prev_pos) && !is_checkmate(prev_pos) && !is_stalemate(prev_pos) &&
                                                                          !qw_lose(prev_pos)) {
                        int prev_num = pos_num[num_of_pos(prev_pos)];
                        positions[i].parents.push_back(prev_num);
                        ++positions[prev_num].depth;
                        positions[prev_num].children.push_back(i);
                    }
                }
            }
        }
    }

    while(!q.empty()) {
        int cur_pos = q.front();
        q.pop();
        if (positions[cur_pos].children.size() > 0) {
            positions[cur_pos].distance = 0;
            for (int child : positions[cur_pos].children) {
                if (positions[child].distance + 1 > positions[cur_pos].distance) {
                    positions[cur_pos].distance = positions[child].distance + 1;
                }
            }
        }
        for (int parent : positions[cur_pos].parents) {
            if (positions[parent].res == Undefined) {
                positions[parent].res = Win;
                positions[parent].distance = positions[cur_pos].distance + 1;
                for (int grandpa : positions[parent].parents) {
                    if (--positions[grandpa].depth == 0) {
                        positions[grandpa].res = Lose;
                        q.push(grandpa);
                    }
                }
            } else {
                positions[parent].distance = std::min(positions[parent].distance,
                                                      positions[cur_pos].distance + 1);
            }
        }
    }
}

int Game::DistanceToCheckmate(char qw_x, char qw_y, char kb_x, char kb_y, bool black_move) {
    vertex pos(qw_x, qw_y, kb_x, kb_y, black_move);
    if (!is_valid(pos)) {
        return -1;
    }
    int num = pos_num[num_of_pos(pos)];
    if (positions[num].res == Win) {
        return positions[num].distance;
    } else {
        return -1;
    }
}

int main() {
    string pos1, pos2;
    cin >> pos1 >> pos2;
    char qw_x = pos1[0] - 'a', qw_y = pos1[1] - '1', kb_x = pos2[0] - 'a', kb_y = pos2[1] - '1';
    Game game;
    int answer = game.DistanceToCheckmate(qw_x, qw_y, kb_x, kb_y, false);
    if (answer == -1) {
        cout << "IMPOSSIBLE" << endl;
    } else {
        cout << answer << endl;
    }

    return 0;
}