#include <iostream>
#include <vector>
//#define _USE_MATH_DEFINES
#include <cmath>

using std::vector;
using std::cin;
using std::cout;
using std::endl;

struct Point {
    double x;
    double y;

    Point() : x(0), y(0) {}

    Point(double x_, double y_) : x(x_), y(y_) {}

    Point operator+(Point other) {
        return Point(x + other.x, y + other.y);
    }

    Point operator-(Point other) {
        return Point(x - other.x, y - other.y);
    }

    Point operator*(double n) {
        return Point(x * n, y * n);
    }
};

/*
 * Функция ниже возвращает true, если полярный угол вектора с началом в точке p1_s и концом
 * в точке p1_f меньше чем полярный угол вектора с началом в точке p2_s и концом в точке p2_f.
 */

double first_angle_less(Point& p1_s, Point& p1_f, Point& p2_s, Point& p2_f) {
    Point v1 = p1_f - p1_s;
    Point v2 = p2_f - p2_s;

    double mult = v1.x * v2.y - v2.x * v1.y;

    return mult > 0;
}

/*
 * Функция ниже проверяет точки из массива на принадлежность одной прямой.
 */

bool is_straight(const vector<Point>& points) {
    Point p1 = points[0];
    Point p2 = points[1];
    double a = p1.y - p2.y;  //ax + by + c = 0
    double b = p2.x - p1.x;
    double c = (-1) * (a * p1.x + b * p1.y);
    bool result = true;
    for (int i = 2; i < points.size(); ++i) {
        if (a * points[i].x + b * points[i].y + c != 0) {
            result = false;
            break;
        }
    }
    return result;
}

struct Polygon {
    vector<Point> vertices;

    Polygon(vector<Point>& v) : vertices(v) {}

    size_t size() const {
        return vertices.size();
    }

    void turn() { //разворот массива
        for (int i = 0; i < vertices.size() / 2; ++i) {
            std::swap(vertices[i], vertices[vertices.size() - i - 1]);
        }
    }

    void find_start() { //делает первым элементом самую нижнюю левую точку, не изменяя порядок обхода
        int i = 0;
        for (int k = 1; k < vertices.size(); ++k) {
            if (vertices[k].y < vertices[i].y ||
                    (vertices[k].y == vertices[i].y && vertices[k].x < vertices[i].x)) {
                i = k;
            }
        }
        vector<Point> vertices_in_new_order;
        for (int j = 0; j < vertices.size(); ++j) {
            vertices_in_new_order.push_back(vertices[(i + j) % size()]);
        }
        vertices = vertices_in_new_order;
    }

    bool is_inside(Point p) const { //проверка на принадлежность точки многоугольнику
        if (is_straight(vertices)) {
            Point p1 = vertices[0];
            Point p2 = vertices[1];
            double a = p1.y - p2.y;  //ax + by + c = 0
            double b = p2.x - p1.x;
            double c = (-1) * (a * p1.x + b * p1.y);
            return a * p.x + b * p.y + c == 0;
        }
        bool result = false;
        int j = static_cast<int>(size() - 1);
        for (int i = 0; i < size(); i++) {
            bool side_to_left = vertices[i].x + (p.y - vertices[i].y) / (vertices[j].y - vertices[i].y) *
                                              (vertices[j].x - vertices[i].x) < p.x;
            bool direction = vertices[i].y < p.y && vertices[j].y >= p.y ||
                    vertices[j].y < p.y && vertices[i].y >= p.y;
            if (side_to_left && direction) {
                result = !result;
            }
            j = i;
        }
        return result;
    }
};

Polygon MinkowskiAdd(Polygon& V, Polygon& W) {
    int i = 0;
    int j = 0;
    vector<Point> result;
    V.turn();
    W.turn();
    V.find_start();
    W.find_start();

    while (i < V.size() && j < W.size()) {
        result.push_back(V.vertices[i] + W.vertices[j]);
        if (first_angle_less(V.vertices[i], V.vertices[i + 1], W.vertices[j], W.vertices[j + 1])) {
                ++i;
        }
        else if (first_angle_less(W.vertices[j], W.vertices[j + 1], V.vertices[i], V.vertices[i + 1])) {
                ++j;
        }
        else {
            ++i;
            ++j;
        }
    }
    if (i < V.size()) {
        while (i < V.size()) {
            result.push_back(V.vertices[i] + W.vertices[0]);
            ++i;
        }
    }
    else if (j < W.size()) {
        while (j < W.size()) {
            result.push_back(V.vertices[0] + W.vertices[j]);
            ++j;
        }
    }

    return Polygon(result);
}

int main() {

    int n, m;
    vector<Point> v, w;

    cin >> n;

    for (int i = 0; i < n; ++i) {
        double x, y;
        cin >> x >> y;
        v.push_back(Point(x, y));
    }

    cin >> m;

    for (int i = 0; i < m; ++i) {
        double x, y;
        cin >> x >> y;
        w.push_back(Point(x * (-1), y * (-1)));
    }

    Polygon V(v);
    Polygon W(w);

    Polygon minkowski_add = MinkowskiAdd(V, W);

    Point zero;

    if (minkowski_add.is_inside(zero)) {
        cout << "YES" << endl;
    }
    else {
        cout << "NO" << endl;
    }

    return 0;
}